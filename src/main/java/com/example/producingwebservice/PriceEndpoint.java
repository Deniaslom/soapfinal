package com.example.producingwebservice;

import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PriceEndpoint {

    private static final String NAMESPACE_URI =
            "http://spring.io/guides/gs-producing-web-service";

    private PriceRepository priceRepository;

    @Autowired
    public PriceEndpoint(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPricesByIdProductRequest")
    @ResponsePayload
    public GetPricesResponse getById(@RequestPayload GetPricesByIdProductRequest request) {
        GetPricesResponse response = new GetPricesResponse();
        response.getPricelist().addAll(priceRepository.getPricesByIdProduct(request.getPriceId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPricesByCurrencyRequest")
    @ResponsePayload
    public GetPricesResponse getPricesByCurrency(@RequestPayload GetPricesByCurrencyRequest request) {
        GetPricesResponse response = new GetPricesResponse();
        response.getPricelist().addAll(priceRepository.getPricesByCurrency(request.getCurrency().toString()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPricesByPriceRangeRequest")
    @ResponsePayload
    public GetPricesResponse getPricesByPriceRange(@RequestPayload GetPricesByPriceRangeRequest request) {
        GetPricesResponse response = new GetPricesResponse();
        response.getPricelist().addAll(priceRepository.getPricesByPriceRange(request.getFrom(),
                request.getTo(),
                request.getCurrency().toString()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deletePriceByCurrencyAndProductRequest")
    @ResponsePayload
    public void getPricesByPriceRange(@RequestPayload DeletePriceByCurrencyAndProductRequest request) {
        priceRepository.deletePriceByCurrencyAndProduct(request.getCurrency().toString(), request.getIdproduct());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "savePriceRequest")
    @ResponsePayload
    public GetPriceResponse save(@RequestPayload SavePriceRequest request) {
        GetPriceResponse response = new GetPriceResponse();
        response.setPrice(priceRepository.save(request.getPrice()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPricesByPriceMoneyAndCurrencyRequest")
    @ResponsePayload
    public GetPricesResponse getPricesByPriceMoneyAndCurrency(@RequestPayload GetPricesByPriceMoneyAndCurrencyRequest request) {
        GetPricesResponse response = new GetPricesResponse();
        response.getPricelist().addAll(priceRepository.getPricesByPriceMoneyAndCurrency(request.getCurrency().toString(), request.getValue()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveListPricesRequest")
    @ResponsePayload
    public void saveListPrices(@RequestPayload SaveListPricesRequest request) {
        priceRepository.saveListPrices(request.getPricelist());
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "clearMapRequest")
    @ResponsePayload
    public void clearMap(@RequestPayload ClearMapRequest request) {
        priceRepository.clearMap();
    }
}