package com.example.producingwebservice;

import io.spring.guides.gs_producing_web_service.Price;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class PriceRepository {

    private static final Map<Long, List<Price>> prices = new HashMap<>();


    public List<Price> getPricesByIdProduct(Long idProduct) {
        return prices.get(idProduct);
    }

    public List<Price> getPricesByCurrency(String currency) {
        return  prices.values().stream()
                .flatMap(Collection::stream)
                .filter(price -> price.getCurrency().toString() == currency)
                .collect(Collectors.toList());
    }

    public List<Price> getPricesByPriceRange(Double from, Double to, String currency) {
        return prices.values().stream()
                .flatMap(Collection::stream)
                .filter(price -> price.getCurrency().toString() == currency)
                .filter(price -> (price.getValue() >= from && price.getValue() <= to))
                .collect(Collectors.toList());
    }

    public void deletePriceByCurrencyAndProduct(String currency, Long idProduct) {
        List<Price> priceList = getPricesByIdProduct(idProduct);
        if (priceList != null) {
            List<Price> newPriceList = priceList.stream().filter(price -> price.getCurrency().toString() != currency).collect(Collectors.toList());
            prices.put(idProduct, newPriceList);
        }
    }

    public Price save(Price price) {
        Price result = null;

        if(prices.containsKey(price.getIdproduct())) {
            deletePriceByCurrencyAndProduct(price.getCurrency().toString(), price.getIdproduct());
            prices.get(price.getIdproduct()).add(price);
            result = price;
        } else{
            prices.put(price.getIdproduct(), Collections.singletonList(price));
        }

        return result;
    }

    public List<Price> getPricesByPriceMoneyAndCurrency(String currency, double value){
        return prices.values().stream()
                .flatMap(Collection::stream)
                .filter(price -> price.getCurrency().toString() == currency)
                .filter(price -> (price.getValue() >= value-1 && price.getValue() <= value+1))
                .collect(Collectors.toList());
    }

    public void saveListPrices(List<Price> priceList){
        priceList.stream().forEach(prise -> save(prise));
    }

    public void clearMap(){
        prices.clear();
    }


}
